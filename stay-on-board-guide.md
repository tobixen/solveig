# Guide for staying on board Solveig

## Checklist on arrival

* Open the gas regulator in the anchor box on the bow of the ship.
* Ventilate if there is much smell.  Quite much of it may come from the toilet and the holding tank.
* Eventually, open the stopcocks on the toilets and engine cooling
* Switches to be turned on at the switchboard: "cabin light", "WC", "water systems", "DC outlets", "fridge".
* There may be some pumping noise when turning on the water systems, but it should stop - if it continues pumping for more than a minute, something is wrong.
* Go through "daily checklist" and eventually "monthly checklist".

## Daily checklist

* Check the battery voltage on the voltmeter. (see also "daily checklist"). Note that there are two battery banks - use the arrow key to scroll.  Acceptable voltage levels are 12.3V - 13.0V if not being connected to land power, and 13.0V-14.8V if connected to land power.
* Check the room under the stairs between the upper and lower saloon; it should be dry.  If it's a little bit of water there, dry up with a textile, sponge or paper.  If it's a lot of water there, use the pump, the water may be pumped into the grey-water tank.  If it's wet, make some notes - are the water tanks empty, full or half-full?  Have there been lots of waves or keeling?  To which side was the keeling?  (It's useful for pinpointing the reason for the water leakages).
* Check that the switch for the gas is turned off behind the stove (unless the stove is in use)
* Check that all doors are either closed or properly opened
* Let the anchor box be open on sunny days, close it on rainy days

## Weekly checklist

* Grey water tank - open it, if it stinks then it should be cleaned.
* Open the battery room / storage room in upper saloon, in front of the engine room.  If there is a rotten/humid smell, then try to ventilate or run the dehumidifying machine there over the night.
* Check that there aren't any water, oil or other fluids in the engine room
* Check that there are no water under the beds in the cabins

## Checklist when leaving the boat

* Look over daily and weekly checklists
* Add some chemicals to the toilet so it won't stink.
* Take out thrash
* If winter: Check that all the heaters are turned on and working
* If winter: ensure all doors are closed.  In summer time they should stay open.
* Turn off most of the electricity:
    * 220V inverter (under bed fore)
    * The switches on the switch board, including cabin light, WC, water systems.
    * DC-outlets
    * Fridge?
    * The switch for the automatic bilge pumps should be left on.
* Lock the boat
* Take down the flag
* Close the regulator for the gas in the bow of the boat
* Check that fenders and mooring ropes are OK.  Consider that the wind direction may change and that the weather may become stormy.

### If the boat will be unattended for more than a week
* Check fridge and food storages, including bread drawer
* Leave some of the floor tiles open for ventilation.
* It's possible to replace the standing flush water in the tubes in the aft toilet with fresh water, this is a good idea
* Close stopcocks for the two toilets plus engine cooling water.
* Disconnect the starter engine powes

## Electricity, 12v

### Switches

There are buttons for most of the electricity on the switchboard over the map reading desk in the upper saloon.  Remarks:

* At the top left there is a down-up-switch for "DC outlets".  This includes power to the internet router, to the TV and the 12V-sockets.
* There is one switch panel with left-right-switches.
* There are also some in-out-buttons.  Some of them are unmarked, including the one for automatic bilge pumps and a heat exchange fan for heating the cabin from the excess engine heat.

### USB charging

There are three ways to charge USB-devices:

* Dedicated USB charging stations - there is one on the port side of the aft bed (connected to the "cabin light" circuit), and one on the port side of the bed in the bow.  The latter may or may not work (that's a TODO-item - figure out if it works or not, and fix it if it doesn't work).  There is also a Clas-Ohlson-splitting-device on the map reading desk with three 12V-outlets and one USB outlet.
* USB-charging-plugs that can be plugged into the 12V-outlets.  There should be some of those lying in front of the map reading desk.  USB outlets can be found:
    * At the starboard side, in the lower saloon, in one of the closets there.
    * At the starboard side, under the map reading desk
    * To the starboard side of the map reading desk (two sockets, one of them usually hosting a splitter and the other usually hosting the TV)
    * In the aft cabin, starboard side, below the seat
    * In the aft cabin, starboard side, by the bed
* Using land-power and a standard 230V charger.

Unfortunately, there currently aren't any sockets in the cockpit.  It's possible to use a USB battery or to use the prolonging cord from the aft cabin bed socket.

### Battery banks

Currently there are two new batteries in front, under the bed there (anchor winch, bow truster), and four old rotten batteries in the main battery room.  There is also one starter battery in the main battery room.

### Switches in the battery room (upper saloon, in front of the engine room)

There are some red big switches in the battery room, on the starboard side:

* Engine - ensures the engine controls and starting engine is getting power from the starting battery.  Turn it off to make it harder to steal the boat.
* "Emergency parallell" - connects that starter battery with the consumption battery.  There is also a charging relay that is supposed to do this job when the starter battery is being charged.
* "House" - gives power to cabin lights, water systems, etc, etc
* Fore - connects the main battery bank with the battery bank under the beds in the front cabin (I'm planning to fix an automatic relay here so that not all the batteries will be drained in case of short circuits, low voltage, etc)
* Aft - gives power to the rear anchor winch and main cockpit winch

### Solar panels

Solar panels currently doesn't give much charge, not even enough to run the fridge.  This should be investigated.

## Electricity, 230v

There is a switch panel under the map reading table; the main switch to the port should always be in up-position.  There will be an indicator LED-light lit if there is 230V.  Most of the AC outlets are (only) going through this main fuse.  At the starboard side there are two switches controlling the AC-outlets for the battery chargers and hot water tank.  Those should be down (OFF) when getting 230V from the battery bank.  There are indicator lights when there is electricity and they are on.

### AC outlets

* Under the bed, midship, in the fore cabin (connected to the battery charger circuit.  There is also a plug there to connect a spare battery charger)
* In the front bathroom
* In the pantry (right at the other side of the wall as compared to the bathroom)
* Under the map reading table (port side of it) - two sockets
* Under the floor, starboard side, for the battery charger
* Under the floor, port side, for the water heater tank
* Under the floor, midship, engine room - for the anti-frost heater (probably connected to the hot water tank circuit).
* Aft toilet (port side of the shower, underside) - connected to the hot water tank circuit (by accident).

### Connecting land power

* There is an orange electricity cord in the anchor box in the bow.  There should be some interface plugs in the box in the cockpit, port side.
* Check that the cord inside the starboard wardrobe in the fore cabin is properly connected
* Check that all switches in the fuse box under the map-reading table is turned up

### Running the AC-electricity from the battery bank

* Ensure the to starboard switches under the map reading table is off/down (hot water tank, battery charger)
* Find the switch on the inverter and turn it on
* Disconnect the land-power-cord in the starboard wardrobe in the fore cabin, and connect the cable from the inverter (currently there is a long black cord in a box there for this purpose.  Keep the wardrobe door closed by using an orange rubber band)
* Make sure to switch off the inverter when it's not in use
* Check the battery voltage frequently.
 
## Internet

We have metered mobile Internet, ESSID solveig.oslo.no, passhprase shtandart.  There is some prepaid quota pr month, after that it's possible to extend the quota with 40 NOK/GB.  Without extention the bandwidth is throttled (often to the point that the connection is rendered useless).

## Water

### Main tanks

We have four big water tanks on starboard and port side.  Do not overfill them, then there will be water leakages.

Under the stairs between the upper and lower saloon, there are handles for closing or opening the connection between the tanks and the water pump.  One would usually want to have all handles open while staying in harbour.  If one knows one is going to sail a lot with the wind i.e. from the starboard side, then it may be an idea to empty the port tanks first, etc.

Water is generally potable while staying in Norway and Sweden, but it usually detoriates quickly when we're mixing hard and soft water.

### Extra tank

There is a small extra tank on the starboard side, connected to a hand-pump in the pantry.  The previous owner had the idea of buying drinking water on bottles and pour it over to the tank.  I somehow don't believe that is a very good idea.  The water tank is mostly used as emergency water if the main tanks are empty.  The water is quite stale by now.
 
### Bottles

I usually keep some spare water bottles under the kitchen sink, those are with tap water, and are generally potable as long as the water is replaced every now and then.  It's also a good idea to keep a water bottle in the fridge.

### Hot water

The hot water tank is on the port side, under the upper saloon.  It can be powered by the engine and by electricity - and it's small, hence, when lying by anchor one may easily run out of hot water.

### Filling water

There are openings on the port side of the boat for filling water.  The aft opening is for the port tank?  (TODO: verify).  (Norwegian mnemonic: Bakre åpning for Babord tank ... or was it Rear Opening for Right Tank?)

## Toilets

It has become a big problem now that the salty water in the inlet tubes get rotten very quickly - maybe even less than 24 hours.  Hence both toilets needs to be used frequently, or it will smell a lot when flushing.

### Aft toilet

Toilet goes right into sea without any holding tank and without any churning.  Don't use toilet paper in it and don't do big things in it, unless being far away from land.

There is a switch for toggling emptying (without water) and flushing (with water).  To prevent smell and to prevent urine to crystalize in the pump and tubes, flush very well.  I usually do twenty strokes with water.

There are seacocks in the closet under the sink.  There is also a tube that can be used for replacing the flushing water with fresh water.

### Fore toilet

The toilet has three buttons - filling with flushing water, emptying and doing both simultaneously.  Usually you will mostly use the top button - but add some extra water when flushing solids.  Flush sufficiently so there won't be sewage standing in the tubes.

There is a holding tank, but the holding tank has no indicator on weather it's full or not.  When it's full, the waste water runs out through the ventilation tube/hole.

The toilet is churning the waste, but the churning pump is very sensitive.  If disposing kitchen paper, tampoons, or almost any other object that does not belong in a toilet, the pump will fail, and it will be needed to disassemble the whole thing - a very dirty and cumbersome work.  Even pubal hair is bad, it will accumulate and cause the pump to gradually fail.

There are stopcocks under the floor.  Right (fore): stopcock for releasing the tank to the sea.  Middle (thinner): stopcock for the flushing water.  Left (aft): stopcock for emptying to land pumping station.

When emptying to land, when most of the tank has been emptied, flush the toilet a lot (like, several minutes).  It's also nice to flush some freshwater into the sewage pipe and into the ventilation tube.

