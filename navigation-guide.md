# Guide for navigating Solveig

## Need-to-know checklist

Everyone should know ...

* ... where to find the "standby"-button on the autopilot and what it's useful for
* ... what an "accidental jibe" is and how to prevent it
* ... where to find the emergency button on the VHF radio
* ... how to put the engine gear in forward, neutral and reverse
* ... how to roll in the genoa
* ... what to do in case of a man-over-board-situation
* ... where to find fire extinguishers, and where to find life wests

## Checklist before departure

* Did you throw garbage, fill water, etc?
* Verify that the stopcocks for the engine cooling water is open
* Make sure either the port or starboard water tanks are closed
* Turn on autopilot (two pull-switches)
* Turn on VHF radio
* Check that the dhingy engine is lifted up
* Turn on navigation lights?
* Check the wind direction and eventually current, on tricky departures, make sure everyone onboard has understood the departure plan.
* Remember to disconnect all the ropes - in particular, remember the orange one ... the electricity cord.

## Checklist after departure

* Give the dhingy more rope.
* Collect all ropes on deck, put the electric cord back into the anchor box ... before it falls into the sea and gets into the propellor
* Collect fenders
* Check that there are no loose items on deck or on the tables in the saloons.

## Checklist on the go

* Check that you aren't crashing with anything - particularly fishing equipment (particularly in Denmark, close to land), other sail boats, ferries, land, etc
* Check the depth
* Radar warning zone may be useful
* Depth alarm may be useful
* If the wind speed picks up, it may be needed to reef (furl) the sails

## Checklist before arrival
* Fenders out
* Ropes ready
* Pull inn the dhingy.  Sometimes it may be smart to put the engine down in the water to prevent the propellor from banging into other boats or the quay.
* Turn on the bow thruster and verify that it's working.

## Mooring up
* Don't trust the bow thruster - it's useful when things are getting out of control, but it's usually a good idea to plan not to use it.
* When going in reverse, the boat will rotate a bit in a clockwise direction, hence it's generally smarter to moore up with the port side towards the quay.
* If the wind isn't too strong or if it's tailwind, start by connecting the stern line, give it at least five metres.  When it's connected, give some engine speed forward (full port), and the ship will move towards the quay.  Adjust the rudder to make the boat parallell with the quay.
* If it's a strong headwind, start with the head line.
* Give enough length on the ropes.  Short brest lines without any dampening will cause jerks, those jerks may keep you awake during the night, and may even destroy the rope, the cleats, etc
* Check the dhingy.  Sometimes it may be smart to connect it to land so it won't rub against the varnish.

## Checklist after arrival
* Turn off navigation lights and engine
* Check the fenders.
* Connect electricity

## Radio

We have two VHF radios on board, a hand-held and a stationary.  The stationary has the antenna in the top of the mast, this gives very good range - maybe a bit too good sometimes, it's mandatory to listen to channel 16, but with the antenna at the top of the boat there may be a bit too much noise on channel 16.  Sometimes we turn off the big radio and uses only the small one.  Only the big one has emergency-button though.

If the stationary radio suddenly makes really loud noises, it's most likely a Digital Selective Call (DSC).  That's either someone wanting exactly your attention, or someone that has pressed the emergency button.

You can monitor both channel 16 and another channel at the same time, choose the other channel you want to listen to, at the small radio hold the "scan"-button in for some seconds, at the big radio do a quick press on the "scan"-button.

## Diesel

There are two tanks of diesel, starboard and port.

Under the stairs between the upper and lower saloon the tanks are connected (three cocks, one for releasing the diesel i.e. to check if there is water or junk in the bottom of the tanks, and two for connecting the tanks.  Leave them open for an hour to equalize the tanks.

Press the "instruments" switch on the switchboard to see how much fuel is left.  The "water"-gauge sometimes shows the fuel level on one of the tanks and sometimes it doesn't work, the "fuel"-gauge shows the fuel level on the other tank.

Inside the engine room there are four cocks, to change the tank used by the engine, reverse all four cocks.  It's cocks for fuel into the engine and return fuel flow, if the return flow doesn't go to the same tank as the fuel is coming from, the tank will go empty rather quickly (and if the tanks are more than half-full, there will be a fuel overflow).

## Genoa

Genoa is pretty worn out, should be careful with it - it will easily rip if it's not reefed (furled) in bad weather.

The sheet car should be at the aft end of the rail when the sail is fully furled out, and in the fore end of the rail when the sail is tightly reefed.  The sheet car cannot be moved when there is tension on the sheet line, eventually it may be needed to tack to change its position.

Remember to switch the lanterns when changing between sail and engine.  Due to problems with the port/starboard red/green lantern, it's best to use the tricolor when sailing.

## Main sail

Main sail is very difficult to get in and out (only solution seems to be to buy a new sail), hence it's usually only used for longer sailing trips.  Sail rolls easier when the boom is pointing port than when it's pointing starboard - particularly when rolling in.

### Taking it out

* Open the rope break for the blue rope and check that the rope runs freely
* Disconnect the boom from the holding ropes before pulling out the sail.
* Use hand-force on the rope at the end of the boom to pull out the first few metres of sail.
* If it gets stuck, get out on deck and try to resolve the problems there.  Alternatively, try to roll it in again and then out.

### Taking it in

Winch in the blue rope - but keep the sail quite tight while rolling in the sail, or it will get stuck next time it's rolled out.  If there is significant wind, go against the wind, or with the wind a little bit from the starboard.

### Preventer

Always use a preventer rope when having tail wind.  There is a rope connected to the boom, it can be connected forward to the cleat in the bow.  Eventually it can also be streched in return back to the cockpit area.  Jibing can be done in a controlled manner by using the preventer rope.

